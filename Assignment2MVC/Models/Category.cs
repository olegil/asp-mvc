﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment2MVC.Models
{
    public class Category
    {
        public virtual int CategoryID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual List<Book> Books { get; set; }
    }
}