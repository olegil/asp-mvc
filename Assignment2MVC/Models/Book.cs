﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment2MVC.Models
{
    public class Book
    {
        public virtual int BookID { get; set; }
        public virtual int AuthorID { get; set; }
        public virtual int CategoryID { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal Price { get; set; }
        public virtual Author Author { get; set; }
        public virtual Category Category { get; set; }
    }
}