﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment2MVC.Models
{
    public class Author
    {
        public virtual int AuthorID { get; set; }
        public virtual string Name { get; set; }
    }
}