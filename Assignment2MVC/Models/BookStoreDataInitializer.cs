﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Assignment2MVC.Models
{
    public class BookStoreDataInitializer : DropCreateDatabaseAlways<BookManagerDBContext>
    {

        protected override void Seed(BookManagerDBContext context)
        {
        

            context.Books.Add(new Book
            {
                 Title = "Macbeth",
                Author = new Author { Name = "William Shakespear" },
                Category = new Category { Name = "Plays" },
                Price = 10.99m
               
            });

            base.Seed(context);

            context.Books.Add(new Book 
            { Author = new Author { Name = "Charles Dickens" }, 
            Category = new Category { Name = "Non Fiction" }, 
            Price = 15.99m, Title = "A tale of two cities" });




        }
    }
}